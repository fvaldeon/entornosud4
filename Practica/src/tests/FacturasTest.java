package tests;

import static org.junit.Assert.*;

import org.junit.BeforeClass;

import org.junit.Test;

import clases.Factura;
import clases.GestorContabilidad;

public class FacturasTest {
	
	static Factura facturaPrueba;
	static GestorContabilidad gestor;
	
	@BeforeClass
	public static void prepararClasePruebas(){
		facturaPrueba = new Factura();
		gestor = new GestorContabilidad();
	}
	
	
	@Test
	public void testCalcularPrecioProducto1(){
		facturaPrueba.setCantidad(3);
		facturaPrueba.setPrecioUnidad(2.5F);
		
		float esperado = 7.5F;
		float actual = facturaPrueba.calcularPrecioTotal();
		
		assertEquals(esperado, actual, 0.0);	
	}
	
	@Test
	public void testCalcularPrecioCantidadCero(){
		facturaPrueba.setCantidad(0);
		facturaPrueba.setPrecioUnidad(2.5F);
		
		float esperado = 0.0F;
		float actual = facturaPrueba.calcularPrecioTotal();
		
		assertEquals(esperado, actual, 0.0);
	}
	
	@Test
	public void testCalcularPrecioConPrecioNegativo(){
		facturaPrueba.setCantidad(2);
		facturaPrueba.setPrecioUnidad(-1.5F);
		
		float esperado = -3.0F;
		float actual = facturaPrueba.calcularPrecioTotal();
		
		assertEquals(esperado, actual, 0.0);
	}
	
	
	@Test 
	public void comprobarFacturaInexistente(){
		GestorContabilidad gestor = new GestorContabilidad();
		Factura esperada = gestor.buscarFactura("12345");
		assertNull(esperada);
	}
	
	@Test 
	public void comprobarFacturaExistente(){
		
		Factura esperada = new Factura();
		esperada.setCodigoFactura("1234");
		gestor.getListaFacturas().add(esperada);
		
		Factura actual = gestor.buscarFactura("1234");
		assertSame(esperada,actual);
	}
	
}
