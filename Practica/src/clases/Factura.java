package clases;

import java.time.LocalDate;

public class Factura {
	private String codigoFactura;
	private LocalDate fecha;
	private String nombrerProducto;
	private float precioUnidad;
	private int cantidad;
	private Cliente cliente;
	
	public Factura(){
		
	}
	
	public Factura(String codigoFactura, LocalDate fecha, String nombrerProducto, float precioUnidad, int cantidad,
			Cliente cliente) {
		this.codigoFactura = codigoFactura;
		this.fecha = fecha;
		this.nombrerProducto = nombrerProducto;
		this.precioUnidad = precioUnidad;
		this.cantidad = cantidad;
		this.cliente = cliente;
	}

	public String getCodigoFactura() {
		return codigoFactura;
	}

	public void setCodigoFactura(String codigoFactura) {
		this.codigoFactura = codigoFactura;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public String getNombrerProducto() {
		return nombrerProducto;
	}

	public void setNombrerProducto(String nombrerProducto) {
		this.nombrerProducto = nombrerProducto;
	}

	public float getPrecioUnidad() {
		return precioUnidad;
	}

	public void setPrecioUnidad(float precioUnidad) {
		this.precioUnidad = precioUnidad;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public float calcularPrecioTotal(){
		return cantidad * precioUnidad;
	}
	
}
