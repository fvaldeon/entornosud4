package clases;

public class Metodos {

	public static double potencia(int base, int exponente){
		int resultado = 1;
		for (int i = 0; i < exponente; i++) {
			resultado = resultado * base;
		}
		return resultado;
	}
	
	public static int redondear(double decimal){
		int valor = (int)decimal;
		if(decimal - valor < 0.5){
			return valor;
		}
		return valor + 1;
		
	}
	
}
