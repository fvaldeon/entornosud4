package clases;

import java.util.ArrayList;

public class GestorContabilidad {
	private ArrayList<Cliente> listaClientes;
	private ArrayList<Factura> listaFacturas;
	
	public GestorContabilidad(){
		listaClientes = new ArrayList<>();
		listaFacturas = new ArrayList<>(); 
	}

	//Metodo Temporal para facilitar las pruebas
	public ArrayList<Cliente> getListaClientes() {
		return listaClientes;
	}

	//Metodo Temporal para facilitar las pruebas
	public ArrayList<Factura> getListaFacturas() {
		return listaFacturas;
	}
	
	
	public Cliente buscarCliente(String dni){
		for (Cliente cliente : listaClientes) {
			if(cliente.getDni().equals(dni)){
				return cliente;
			}
		}
		return null;
	}
	
	public Factura buscarFactura(String codigo){
		if(listaFacturas.isEmpty()){
			return null;
		}
		return listaFacturas.get(0);
	}
}
