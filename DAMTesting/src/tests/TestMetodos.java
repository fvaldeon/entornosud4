package tests;

import static org.junit.Assert.*;

import org.junit.Test;
import clases.Metodos;

public class TestMetodos {
	
	@Test
	public void redondearAlzaPositivo(){
		int actual = Metodos.redondear(7.6);
		int esperado = 8;
		assertEquals(esperado, actual);
	}
	
	@Test
	public void redondearBajaPositivo(){
		int actual = Metodos.redondear(7.3);
		int esperado = 7;
		assertEquals(esperado, actual);
	}
	
	@Test
	public void redondearAlzaNegativo(){
		int actual = Metodos.redondear(-4.7);
		int esperado = -5;
		assertEquals(esperado, actual);
	}
	
	@Test
	public void redondearBajaNegativo(){
		int actual = Metodos.redondear(-0.3);
		int esperado = 0;
		assertEquals(esperado, actual);
	}
	
	@Test
	public void potenciaExponentePositiva(){
		double actual = Metodos.potencia(3,4);
		double esperado = 81;
		assertEquals(esperado, actual, 0);
	}
	
	@Test
	public void potenciaBaseNegativa(){
		double actual = Metodos.potencia(-2,3);
		double esperado = -8;
		assertEquals(esperado, actual,0);
	}
	
	@Test
	public void potenciaExponenteCero(){
		double actual = Metodos.potencia(3,0);
		double esperado = 1;
		assertEquals(esperado, actual,0);
	}
	
	@Test
	public void potenciaExponenteCeroBaseNegativa(){
		double actual = Metodos.potencia(-3,0);
		double esperado = 1;
		assertEquals(esperado, actual,0);
	}
	
	@Test
	public void potenciaExponenteNegativo(){
		double actual = Metodos.potencia(2,-2);
		double esperado = 0.25;
		assertEquals(esperado, actual,0);
	}
	
}
