package clases;

public class Metodos {

	public static double potencia(int base, int exponente) {
		double resultado = 1;
		if (exponente >= 0) {
			for (int i = 0; i < exponente; i++) {
				resultado = resultado * base;
			}
			return resultado;
		}
		for (int i = 0; i < -exponente; i++) {
			resultado = resultado * base;
		}
		return 1 / resultado;
	}

	public static int redondear(double decimal) {
		int valor = (int) decimal;

		if (decimal >= 0) {
			if (decimal - valor < 0.5) {
				return valor;
			}
			return valor + 1;
		} else {
			if (decimal - valor > -0.5) {
				return valor;
			}
			return valor - 1;
		}
	}

	
}
