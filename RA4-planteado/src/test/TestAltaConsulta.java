package test;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import hospital.Consulta;
import hospital.Doctor;
import hospital.Hospital;

public class TestAltaConsulta {

	static Hospital hospital;
	Doctor doctor;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		hospital = new Hospital();
		
	}

	@Before
	public void setUp() throws Exception {
		hospital.getListaDoctores().clear();
		hospital.getListaConsultas().clear();
		doctor = new Doctor("123", "Fer", "Cabecera");
	}

	//alta consulta cuando el doctor no figura en plantilla
	@Test
	public void testAltaConsultaDoctorNofigura() {
		
		Consulta consulta = new Consulta(doctor, null, "Cabecera");
		
		hospital.altaConsulta(consulta);
		
		boolean actual = hospital.getListaConsultas().contains(consulta);
		
		assertFalse(actual);
	}

	//altaConsulta cuando el doctor esta en plantilla
	@Test
	public void testAltaConsultaDoctorSiFigura() {
		hospital.getListaDoctores().add(doctor);
		
		Consulta consulta = new Consulta(doctor, null, "Cabecera");
		
		hospital.altaConsulta(consulta);
		
		boolean actual = hospital.getListaConsultas().contains(consulta);
		
		assertTrue(actual);
	}
	
	//altaConsulta con un doctor de distinta especialidad que la consulta
	@Test
	public void testAltaConsultaEspecialidadDistinta(){
		hospital.getListaDoctores().add(doctor);
		
		Consulta consulta = new Consulta(doctor, null, "Cabecera");
		
		hospital.altaConsulta(consulta);
		
		boolean actual = hospital.getListaConsultas().contains(consulta);
		
		assertFalse(actual);
	}
	
	//AltaConsulta cuando hay una consulta anterior en la misma fecha y con el mismo doctor
	//Compruebo que si se da de alta
	@Test
	public void altaConsultaConOtraAnteriorMismoFecha(){
		hospital.getListaDoctores().add(doctor);
		
		Consulta consulta1 = new Consulta(doctor, null, "Cabecera");
		Consulta consulta2 = new Consulta(doctor, null, "Cabecera");
		hospital.getListaConsultas().add(consulta1);
		
		
		hospital.altaConsulta(consulta2);
		
		boolean actual = hospital.getListaConsultas().contains(consulta2);
		
		assertFalse(actual);
	
	}
	
	
	//AltaConsulta cuando hay una consulta anterior pero en distinta fecha
	//Compruebo que si se da de alta
	@Test
	public void altaConsultaConOtraAnteriorDistintaFecha(){
		hospital.getListaDoctores().add(doctor);
		
		Consulta consulta1 = new Consulta(doctor, null, "Cabecera");
		consulta1.setFecha(LocalDate.parse("2000-05-05"));
		
		Consulta consulta2 = new Consulta(doctor, null, "Cabecera");
		
		hospital.getListaConsultas().add(consulta1);
		
		hospital.altaConsulta(consulta2);
		
		boolean actual = hospital.getListaConsultas().contains(consulta2);
		
		assertTrue(actual);
	
	}
	
}
