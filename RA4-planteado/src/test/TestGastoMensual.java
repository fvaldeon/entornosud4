package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import hospital.Doctor;
import hospital.Hospital;

public class TestGastoMensual {

	static Hospital hospital;
	
	@BeforeClass
	public static void iniciarClase(){
		hospital = new Hospital();
	}
	
	@Before
	public void antesDeMetodos(){
		hospital.getListaDoctores().clear();
	}
	
	//No tengo doctores en el hospital
	@Test
	public void testGastoMensualSinDoctores() {
		double esperado = 0;
		double actual = hospital.gastoMensual();
		
		assertEquals(esperado, actual, 0.0);
	}

	@Test
	public void testGastoMensualConDoctores(){
		hospital.getListaDoctores().add(new Doctor("123", "Fer", "Cabecera"));
		hospital.getListaDoctores().add(new Doctor("1223", "Fran", "Cabecera"));
		
		double esperado = 4000;
		double actual = hospital.gastoMensual();
		
		assertEquals(esperado, actual, 0.0);
	}
	
	@Test
	public void testConCirujanos(){
		hospital.getListaDoctores().add(new Doctor("123", "Fer", "Cirujano"));
		hospital.getListaDoctores().add(new Doctor("1223", "Fran", "Cabecera"));
		
		double esperado = 4200;
		double actual = hospital.gastoMensual();
		
		assertEquals(esperado, actual, 0.0);
	}
	
}
