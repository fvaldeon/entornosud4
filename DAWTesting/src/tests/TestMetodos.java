package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import clases.Metodos;

public class TestMetodos {

	@Test
	public void testRedondearAlzaPositivo(){
		int actual = Metodos.redondear(6.7);
		int esperado = 7;
		assertEquals(esperado, actual);
	}
	
	@Test
	public void testRedondearBajaPositivo(){
		int actual = Metodos.redondear(6.3);
		int esperado = 6;
		assertEquals(esperado, actual);
	}
	
	@Test
	public void testRedondearBajaNegativo(){
		int actual = Metodos.redondear(-2.3);
		int esperado = -2;
		assertEquals(esperado, actual);
	}
	
	@Test
	public void testRedondearAlzaNegativo(){
		int actual = Metodos.redondear(-2.7);
		int esperado = -3;
		assertEquals(esperado, actual);
	}
	
	@Test
	public void testPotenciaBasePositiva(){
		double actual = Metodos.potencia(2, 3);
		double esperado = 8;
		assertEquals(esperado, actual, 0.0000001);
	}
	
	@Test
	public void testPotenciaExponenteCero(){
		fail("no implementado");
	}
	
	@Test
	public void testPotenciaExponenteNegativo(){
		fail("no implementado");
	}
	
	@Test
	public void testPotenciaBaseNegativa(){
		fail("no implementado");
	}
	
	@Test
	public void testPotenciaBaseCero(){
		fail("no implementado");
	}
	
}
