package hospital;

import java.util.ArrayList;

public class Hospital {
	private ArrayList<Doctor> listaDoctores;
	private ArrayList<Paciente> listaPacientes;
	private ArrayList<Consulta> listaConsultas;
	
	public Hospital() {
		listaConsultas = new ArrayList<>();
		listaDoctores = new ArrayList<>();
		listaPacientes = new ArrayList<>();
	}

	public ArrayList<Doctor> getListaDoctores() {
		return listaDoctores;
	}

	public ArrayList<Paciente> getListaPacientes() {
		return listaPacientes;
	}

	public ArrayList<Consulta> getListaConsultas() {
		return listaConsultas;
	}
	
	/*
	 * Métodos a probar
	 */
	
	public void altaConsulta(Consulta consulta) {
		
	}
	
	public double gastoMensual() {
		return 0;
	}
	
}
